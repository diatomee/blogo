# Blogo

*Blogo* est un générateur de blog statique minimal. Il est d’avantage orienté pour une utilisation personnelle et correspond bien aux expressions «tenir un journal», «tenir un carnet», qu’il soit intime, public ou les deux. Il peut également servir de base pour implémenter tout ce que l’on veut.

## Fonctionnalités

- plusieurs types de publications : privées, publiques, cachées, épinglées
- navigation simple entre les différentes publications
- prise en charge des catégories
- modèle de page unique pour toutes les publications
- possibilité d’insérer du JS et du CSS pour une publication spécifique
- un thème CSS simple

## Prérequis

*Blogo* nécessite l’installation de Python 3, *pip* et *Tenjin* pour fonctionner.

### Python 3

Le générateur de *Blogo* est écrit en Python 3. Pour vérifier la présence de Python 3, il faut saisir dans un terminal la commande `python3 -V`. Si un numéro de version dont le premier chiffre vaut 3 s’affiche, c’est tout bon. Sinon, il faut l’[installer](https://www.python.org/downloads/).

### *Tenjin* et *pip*

[*Tenjin*](http://www.kuwata-lab.com/tenjin/) est le moteur de templates choisi par *Blogo* pour sa simplicité et sa rapidité. Il s’installe facilement grâce à *pip*. Voici les deux lignes de commande à saisir l’une après l’autre :

```
sudo apt-get install python3-pip
python3 -m pip install Tenjin
```

## Utilisation

Dans le dossier `content/` se trouve le blog à générer. *Blogo* génère deux blogs; l'un est public (à mettre en ligne) et l'autre est privé (consultable uniquement hors ligne).

La génération se fait en ouvrant un terminal à l’emplacement du fichier `build.py` et en saisissant ceci :

```
python3 build.py
```

**Pour tester rapidement le blog**, il suffit de saisir la commande ci-dessus. Le blog est fourni par défaut avec quelques publications utiles, dont la documentation de *Blogo* ;) Il faut ensuite ouvrir dans un navigateur le fichier `index.html` qui se trouve dans le dossier `secret/`.

Si une erreur se produit, peut-être que Python 3, *pip* ou *Tenjin* ne sont pas correctement installés (Cf. Prérequis). Les dossiers et fichiers principaux de *Blogo* ne peuvent être renommés ou supprimés.

## Améliorations à apporter

- augmenter les ressources d'aide
- améliorer les vues pour montrer le type de publication (masquée, secrète, épinglée)
- rendre multilingue