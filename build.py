# BLOGO : générateur de blog statique opensource et libre.

import errno, os, re, shutil, tenjin
from datetime import datetime
from tenjin.helpers import *
from tenjin.html import *
pp = [
	tenjin.TrimPreprocessor(), # trim spaces before tags
	tenjin.PrefixedLinePreprocessor() # convert ':: ...' into '<?py ... ?>'
]
engine = tenjin.Engine(path=['content/views'], pp=pp)

# Construction du blog suivant le contenu de "content/"
def build(secret = False):
	class B:
		folder = 'secret' if secret else 'public'
		secretBaseURL = f'file://{os.path.abspath("")}/secret/'
		def viewHead(self): return engine.render('head.pyhtml', {'blog': self})
		def viewFoot(self): return engine.render('foot.pyhtml', {'blog': self})
		def removeHTML(self, strHTML):
			tags = [
				{'regex': r"<.*?>", 'substitute': ''}, # tags
				{'regex': r"&nbsp;", 'substitute': ' '}, # &nbsp;
				{'regex': r"&[a-bA-B]{4};", 'substitute': ' '} # tous les html entities
			]
			for t in tags: strHTML = re.sub(t['regex'], t['substitute'], strHTML)
			return strHTML


	# 0. Lecture ou création du fichier 'config'
	if os.path.isfile('content/config'):
		with open('content/config', 'r') as f:
			keys = f.read().split('\n')
			for k in keys:
				if k != '':
					parts = k.split(':', 1)
					setattr(B, parts[0].strip(), parts[1].strip())
			err = []
			if hasattr(B, 'publicBaseURL') == False: err.append('publicBaseURL')
			if hasattr(B, 'title') == False: err.append('title')
			if hasattr(B, 'description') == False: err.append('description')
			if hasattr(B, 'pagination') == False: err.append('pagination')
			if len(err) > 0:
				print('ERREUR: Clef(s) manquante(s) dans le fichier config : ', ','.join(err), '.')
				return ''
			B.baseURL = B.secretBaseURL if secret else B.publicBaseURL
			B.updated = datetime.now()
	else:
		with open('content/config', 'a') as f:
			data = '\n'.join([
				'publicBaseURL: https://.../',
				'title: Titre du blog, sans HTML',
				'description: Description du blog, sans HTML',
				'pagination: 20'
			])
			f.write(data)
		print('Un fichier nommé "config" vient d’être créé dans le dossier "content". Il suffit de l’éditer et de relancer la commande "python3 build.py". Ceci n’est à faire qu’une fois, lors de l’initialisation du blog.')
		return ''

	# 1. Construction du tableau des tags et des posts avec tri par date et slug
	posts, allTags, allPins = [], [], []
	for dirpath, dirnames, files in os.walk('content/posts'):
		for name in files:
			if name.lower().endswith('.html'):
				with open(os.path.join(dirpath, name), 'r') as post:
					p = post.read()
					firstLine = p.split('\n', 1)[0].strip()
					if firstLine[:4].lower() != '<h1>': # clef(s) présente(s)
						keys = firstLine.split(' | ')
						class P:
							secret = False
							mask = False
							pin = False
						path = dirpath.split('/')[2:]
						P.dir = '/'.join(path)
						P.slug = f'posts/{P.dir}/{name}'
						tags = []
						patternDate = re.compile("^([0-2]\d|3[0-1])\/(0\d|1[0-2])\/\d{4}$") #JJ/MM/AAAA
						for k in keys:
							type = k[0]
							if type == '#':
								t = k[1:].strip()
								tags.append(t)
								allTags.append(t)
							elif type == '*': P.secret = True
							elif type == '~':
								P.mask = True
								P.pin = False
							elif type == '!':
								P.pin = True
								P.mask = False
							elif type == '&': P.style = k[1:].strip()
							elif type == '>': P.script = k[1:].strip()
							elif patternDate.match(k.strip()): P.pub = k.strip()
							elif type == '.':
								parts = k.split(':', 1)
								setattr(P, parts[0].strip(), parts[1].strip())
							else: P.title = k.strip()
						P.tags = tags
						if hasattr(P, 'pub') == False:
							d = P.dir.split('/')[::-1]
							if len(d) == 3:
								if len(d[0]) == 1: d[0] = '0' + d[0]
								if len(d[1]) == 1: d[1] = '0' + d[1]
								d = '/'.join(d)
								if patternDate.match(d): P.pub = d
						if hasattr(P, 'title') == False or hasattr(P, 'pub') == False:
							print(f'ERREUR : Titre manquant ou date manquante pour une publication ({P.slug}).')
							return ''
						else:
							content = p.split('\n', 1)[1].strip()


							rgx = re.compile(r'(\{\=.*?\=\})')
							contents = rgx.split(content)
							contentOK = []
							for c in contents:
								if rgx.match(c):
									c = c.replace('blog.', 'B.')
									try:
										contentOK.append(eval(c[2:-2]))
									except:
										contentOK.append(c)
								else:
									contentOK.append(c)


							P.content = ''.join(contentOK)
							if secret == True: posts.append(P)
							elif P.secret == False: posts.append(P)
					else:
						print(f'ERREUR : Clef manquante pour une publication ({P.slug}). Un titre est nécessaire.')
						return ''
	posts = sorted(posts, key=lambda p: p.slug)
	posts = sorted(posts, key=lambda p: p.pub.split('/')[::-1])
	B.posts = posts[::-1] # reverse posts
	allTags = list(set(allTags))
	tagsDict = []
	if os.path.isfile('content/tags'):
		with open('content/tags', 'r') as f:
			tags = f.read().split('\n\n')
			for t in tags:
				elts = t.split('\n')
				class T:
					id = elts[0].strip()
					text = elts[1].strip()
				if len(elts) == 3: T.description = elts[2].strip()
				tagsDict.append(T)
	B.tags = tagsDict
	errs = []
	for p in B.posts:
		postTagsDict = []
		err = []
		for t in p.tags:
			tmp = ''
			for td in tagsDict:
				if t == td.id:
					tmp = 'yo'
					postTagsDict.append(td)
			if tmp == '': err.append({'post': p, 'tag': t})
		if len(err) > 0: errs.append(err)
		p.tags = postTagsDict
		if p.pin == True: allPins.append(p)
	B.pins = allPins
	if len(errs) > 0:
		print(f'INFO : Catégorie(s) manquante(s) dans le fichier "tags", et donc ingorée(s).')
		for e in errs:
			print(f'  - {e[0]["post"].slug} :')
			for e1 in e:
				print(f'    > {e1["tag"]}')

	# 2. Crée ou recrée (en conservant le ".git") le dossier du blog
	if os.path.isdir(B.folder) == False: os.mkdir(B.folder)
	else:
		if os.path.isdir(B.folder + '/.git'):
			shutil.copytree(B.folder + '/.git', '.gitBlogTmp')
		shutil.rmtree(B.folder)
		os.mkdir(B.folder)
		if os.path.isdir('.gitBlogTmp'):
			shutil.copytree('.gitBlogTmp', B.folder + '/.git')
			shutil.rmtree('.gitBlogTmp')
	shutil.copytree('content/posts', B.folder + '/posts')
	
	# 3. Création des pages html...
	# 3.1. ...de chaque page 'post'
	for p in posts:
		try: os.makedirs(B.folder + '/posts/' + p.dir)
		except OSError as e:
			if e.errno != errno.EEXIST: raise
		B1 = B()
		B1.post = p
		with open(B.folder + '/' + p.slug, 'w') as f:
			f.write(engine.render('post.pyhtml', {'blog': B1}))
	# 3.2. ...de chaque page 'posts' par catégories
	for t in B.tags:
		postsByTag = []
		for p in B.posts:
			if t in p.tags:
				postsByTag.append(p)
		B1 = B()
		B1.posts = postsByTag
		B1.tag = t
		nb = int(B.pagination)
		ranges = [B1.posts[i:i+nb] for i in range(0, len(B1.posts), nb)]
		pageNum = 1
		B1.totalPages = len(ranges)
		for r in ranges:
			B1.pageNum = str(pageNum)
			B1.posts = r
			with open(B.folder + '/'+ t.id + '_' + str(pageNum) + '.html', 'w') as f:
				f.write(engine.render('posts.pyhtml', {'blog': B1}))
			pageNum += 1
	# 3.3. ...de la page 'posts' (accueil)
	B1 = B()
	ranges = [B1.posts[i:i+nb] for i in range(0, len(B1.posts), nb)]
	pageNum = 1
	B1.totalPages = len(ranges)
	for r in ranges:
		B1.pageNum = str(pageNum)
		B1.posts = r
		with open(B.folder + '/index' + ('' if pageNum == 1 else '_' + str(pageNum)) + '.html', 'w') as f:
			f.write(engine.render('posts.pyhtml', {'blog': B1}))
		pageNum += 1

	# 4. Copie du dossier 'theme' tel quel
	os.mkdir(B.folder + '/extra')
	if os.path.isdir('content/theme'):
		shutil.copytree('content/theme', B.folder + '/theme')

	# 5. Retourne le tableau des posts, juste pour un affichage de son nombre dans le terminal
	return posts

P = build()

if P != '':
	S = build(secret = True)
	print('\x1b[1;37;41m', f'♥ La blogo estas bela ! {len(S)} paĝo{"j" if len(S) > 1 else ""} ({len(S) - len(P)} {"sekretaj" if len(S) - len(P) > 1 else "sekreto"}). ♥', '\x1b[0m')
